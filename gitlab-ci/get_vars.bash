#!/bin/bash

set -e

thisfile=$(realpath "${BASH_SOURCE[0]}")
thisdir=${thisfile%/*}
[[ -f $thisdir/fetch_resource.bash ]]
[[ -f $thisdir/helpers.bash ]]
[[ -f $thisdir/get_versions.sh ]]
source "$thisdir/helpers.bash"

command -v jq > /dev/null

declare -a versions periods
declare target_index

is_absent_version() {
    is_version "$1" || return 1
    local ver
    for ver in "${versions[@]}"; do
        [[ $1 == "$ver" ]] && return 1
    done
    return 0
}

is_newer() {
    # Bigger numbers are newer (more recent dates)
    local newer
    newer=$(printf '%s\n' "$1" "$2" | sort | tail -n1)
    [[ $newer == "$2" ]] && return 0
    return 1
}

populate_versions() {
    (( $# == 2 ))
    local -n out
    out=$1
    branch=$2
    local cmdline
    cmdline=( bash "$thisdir/get_versions.sh" "$CI_PROJECT_PATH" "$branch" )
    #                                                 shellcheck disable=SC2034
    readarray -t out < <( "${cmdline[@]}" | cut -d" " -f1 | sort -V )
}

find_candidate() {
    local period
    for period in "${periods[@]}"; do
        if is_absent_version "$(period_to_version "$period")"; then
            printf %s "$period"
            return 0
        fi
    done
    return 1
}

determine_period() {
    local target_period
    # Favor explicit period expressed in commit message
    target_period=$(printf %s "$CI_COMMIT_MESSAGE" | awk '/^Period:/ {print $2}')
    # But if there's already a version for it, disregard
    if [[ $target_period ]] && {
        ! is_period "$target_period" ||
        ! is_absent_version "$(period_to_version "$target_period")"
    }; then
        target_period=
    fi

    # Next try oldest period
    if [[ ! $target_period ]]; then
        if (( ${#versions[*]} )); then
            target_period=$(find_candidate) || target_period=${periods[-1]}
        else
            target_period=${periods[0]}
        fi
    fi

    is_period "$target_period"
    printf %s "$target_period"
}

write_out() {
    local target_period target_page target_archive
    target_period=$(determine_period)
    target_version=$(period_to_version "$target_period")
    target_page=$target_index$target_period/
    target_archive=https://lists.gnu.org/archive/mbox/$CI_COMMIT_BRANCH/$target_period
    target_head=${periods[-1]}

    tee "$target_period.env" <<EOF
TARGET_INDEX=$target_index
TARGET_PERIOD=$target_period
TARGET_VERSION=$target_version
TARGET_PAGE=$target_page
TARGET_CSV=$target_period.csv
TARGET_MBOX=$target_period.mbox
TARGET_ARCHIVE=$target_archive
TARGET_HEAD=$target_head
EOF
    ln -sf "$target_period.env" latest.env
}

ensure_resources() {
    if [[ -f index.html ]]; then
        local -i lastmod now
        lastmod=$(date -r index.html +%s)
        now=$(date +%s)
        if (( ( (now - lastmod) / 60 / 60 ) > 4 )); then
            rm -f index.html relpaths periods
        fi
    fi
    if [[ ! -f index.html ]]; then
        bash "$thisdir/fetch_resource.bash" index.html "$target_index"
    fi
    if [[ ! -f relpaths ]]; then
        grep '^<li><b>' index.html | cut -f2 -d'"' > relpaths
        [[ ! -f periods ]]
        # Save periods in ascending order
        cut -f1 -d'/' < relpaths | sort > periods
    fi
}

main() {
    [[ $CI_COMMIT_BRANCH ]]
    [[ $CI_PROJECT_PATH ]]
    target_index=https://lists.gnu.org/archive/html/$CI_COMMIT_BRANCH/

    [[ -d LinkData ]]
    cd LinkData

    ensure_resources
    populate_versions versions "$CI_COMMIT_BRANCH"
    readarray -t periods <./periods
    write_out
}

test_is_absent_version() {
    versions=( "2020.3.0" "2020.4.0" "2020.6.0" )
    _assert_0 is_absent_version "2020.5.0"
    _assert_1 is_absent_version "2020.4.0"
}

test_find_candidate() {
    periods=( "2020-03" "2020-04" "2020-05" "2020-06" )
    #
    versions=( "2020.4.0" "2020.6.0" )
    want=2020-03 _assert_equal find_candidate
    versions=( "2020.3.0" "2020.4.0" "2020.6.0" )
    want=2020-05 _assert_equal find_candidate
    versions=( "2020.3.0" "2020.4.0" "2020.5.0" "2020.6.0" )
    _assert_1 find_candidate
}

test_determine_period() {
    periods=( "2020-03" "2020-04" "2020-05" "2020-06" )
    #
    versions=( "2020.3.0" "2020.4.0" "2020.6.0" )
    CI_COMMIT_MESSAGE="Foo\n\nPeriod: 2020-05"
    want=2020-05 _assert_equal determine_period
    #
    versions=( "2020.4.0" "2020.6.0" )
    CI_COMMIT_MESSAGE="Foo\n\nPeriod: 2020-04"
    want=2020-03 _assert_equal determine_period
    #
    CI_COMMIT_MESSAGE="Foo"
    want=2020-03 _assert_equal determine_period
    #
    versions=( "2020.3.0" "2020.4.0" "2020.6.0" )
    want=2020-05 _assert_equal determine_period
    #
    versions=( "2020.3.0" "2020.4.0" "2020.5.0" "2020.6.0" )
    want=2020-06 _assert_equal determine_period
}

_run() {
    CI_COMMIT_BRANCH=fake
    CI_COMMIT_MESSAGE="This is a fake commit\n\nIndeed."
    (( ! ${#versions[@]} ))
    (( ! ${#periods[@]} ))
    "$@"
    if (( ! fail_count )) ; then
        echo ".ok $*"
    fi
    versions=()
    periods=()
    if (( fail_count )); then
        echo "fail count $fail_count"
        return 1
    fi
    return 0
}

test_all() {
    _run test_is_period
    _run test_is_tag
    _run test_is_version
    _run test_is_own_tag
    _run test_tag_to_period
    _run test_period_to_tag
    _run test_is_absent_version
    _run test_find_candidate
    _run test_determine_period
    _run test_to_version
    _run test_version_to_period
    _run test_period_to_version
}

"$@"
