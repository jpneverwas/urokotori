#!/bin/bash
h__thisfile=$(realpath "${BASH_SOURCE[0]}")
h__thisdir=${h__thisfile%/*}
#                                                     shellcheck disable=SC1090
source "$h__thisdir/helpers.sh"
unset h__thisfile h__thisdir

is_period() {
    [[ $1 =~ ^[[:digit:]]{4}-[[:digit:]]{2}$ ]] && return 0
    return 1
}

is_tag() {
    [[ $1 =~ .+[^-]-([[:digit:]]{4}-[[:digit:]]{2})$ ]] && return 0
    return 1
}

is_version() {
    [[ $1 =~ ^([[:digit:]]{4})[.]([1-9]{1}[0-2]{,1})[.]0$ ]] && return 0
    return 1
}

is_own_tag() {
    [[ $1 =~ $CI_COMMIT_BRANCH-([[:digit:]]{4}-[[:digit:]]{2})$ ]] && return 0
    return 1
}

to_version() {
    if ! res=$(printf %s "$1" | tag2ver) || [[ $res == "$1" ]]; then
        return 1
    fi
    printf %s "$res"
}

version_to_period() {
    if is_version "$1"; then
        local day
        day=$(printf '%02d' "${BASH_REMATCH[2]}")
        printf %s "${BASH_REMATCH[1]}-$day"
        return 0
    fi
    return 1
}

period_to_version() {
    if is_period "$1"; then
        local day
        printf '%s.' "${1%-*}"
        printf -v day '10#%s' "${1#*-}"
        printf %s $(( day ))
        printf %s '.0'
        return 0
    fi
    return 1
}

tag_to_period() {
    if is_tag "$1"; then
        printf %s "${BASH_REMATCH[1]}"
        return 0
    fi
    return 1
}

period_to_tag() {
    if is_period "$1"; then
        printf %s "${CI_COMMIT_BRANCH}-$1"
        return 0
    fi
    return 1
}

# Testing
declare want
declare -i fail_count

_assert_0() {
    if ! "$@"; then
        echo "failed: $*" >&2
        (( ++fail_count ))
        return 1
    fi
    return 0
}

_assert_1() {
    if ( "$@" &>/dev/null ) ; then
        echo "failed: $*" >&2
        (( ++fail_count ))
        return 1
    fi
    return 0
}

_assert_equal() {
    local rv
    if ! rv=$(set -e; "$@"); then
        echo "failed: $* exited nonzero" >&2
        (( ++fail_count ))
        return 1
    fi
    if [[ $rv != "$want" ]]; then
        echo "failed: $* != $want" >&2
        (( ++fail_count ))
        return 1
    fi
    return 0
}

test_is_period() {
    _assert_0 is_period "2020-03"
    _assert_1 is_period "fake-2020-03"
}

test_is_tag() {
    _assert_0 is_tag "fake-2020-03"
    _assert_0 is_tag "foo-2020-03"
    _assert_1 is_tag "2020-03"
}

test_is_own_tag() {
    _assert_0 is_own_tag "fake-2020-03"
    _assert_1 is_own_tag "2020-03"
    _assert_1 is_own_tag "foo-2020-03"
}

test_is_version() {
    _assert_0 is_version "2020.3.0"
    _assert_0 is_version "2020.10.0"
    _assert_1 is_version "2020.03.0"
    _assert_1 is_version "2020.03"
    _assert_1 is_version "foo-2020-03"
}

test_tag_to_period() {
    want=2020-03 _assert_equal tag_to_period "fake-2020-03"
    want='' _assert_1 _assert_equal tag_to_period "2020-03"
}

test_period_to_tag() {
    want=fake-2020-03 _assert_equal period_to_tag "2020-03"
    want=2020-03 _assert_1 _assert_equal period_to_tag "fake-2020-03"
}

test_to_version() {
    want=2020.9.0 _assert_equal to_version fake-2020-09
    want=2020.9.0 _assert_equal to_version 2020-09
}

test_version_to_period() {
    want=2020-03 _assert_equal version_to_period "2020.3.0"
    want=2020-03 _assert_1 _assert_equal version_to_period "2020-03"
}

test_period_to_version() {
    want=2020.3.0 _assert_equal period_to_version "2020-03"
    want=2020.3.0 _assert_1 _assert_equal period_to_version "2020.3.0"
    # This one ignores version as input
    tv1=$(printf %s "2020-03" | tag2ver)
    want=2020.3.0 _assert_equal printf %s "$tv1"
    tv2=$(printf %s "2020.3.0" | tag2ver)
    want=2020.3.0 _assert_equal printf %s "$tv2"
}
