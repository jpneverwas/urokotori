#!/bin/bash

set -e

(( $# == 2 ))
(( tries = 10 ))

while ! {
    curl --proxy "$SOCKS_PROXY" \
    --silent --show-error --fail --location \
    --output "$1" "$2"
} && (( tries-- )); do
    sleep 2
    echo "Failed to fetch $2. $tries tries remaining."
done

(( tries ))
