#!/bin/sh

tag2ver() {
    sed -e 's/.\+-\([[:digit:]]\{4\}-[[:digit:]]\{2\}$\)/\1/' \
        -e 's/\(.\+\)-0\?\([[:digit:]]\{1,2\}\)$/\1.\2.0/'
}

# In bash, we can just do export "${!TARGET_@}"
export_sourced() {
    while read -r file; do
        var=$(printf %s "$file" | cut -f1 -d'=')
        export "${var?}"
    done < "$1"
}

