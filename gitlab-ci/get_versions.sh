#!/bin/sh
#
# Use like:
#
#   $ gitlab-ci/get_versions.sh jpneverwas/urokotori help-gnu-emacs
#
#   2013.10.0 gid://gitlab/Packages::Package/1062429
#   2013.9.0 gid://gitlab/Packages::Package/1062354
#   2013.8.0 gid://gitlab/Packages::Package/1062302

GQURL=https://gitlab.com/api/graphql
project_path=$1
package_name=$2

test -n "$project_path"

collected=$(mktemp)

request=$(
    cat <<EOF
query (\$ref: String!) {
  project(fullPath: "$project_path") {
    packages(packageName: "$package_name", packageType: GENERIC, after: \$ref) {
      nodes {
        version
        id
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
}
EOF
)

do_one() {
    response=$(
        printf %s "$request" | \
            jq -R --slurp "{query:., variables: {\"ref\": \"$1\"}}" | \
            curl "$GQURL" \
                --silent --show-error --fail --location \
                --header "Content-Type: application/json" \
                --request POST --data @-
    )
    printf %s "$response" | \
        jq -r '.data.project.packages.nodes | .[] | .version + " " + .id' >> "$collected"
    printf %s "$response" | \
        jq -r '.data.project.packages.pageInfo
            | if .hasNextPage then .endCursor else "" end'
}

next=$(do_one "")
while test -n "$next"; do
    next=$(do_one "$next")
done
cat "$collected"
rm "$collected"
