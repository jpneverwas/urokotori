#!/bin/bash

set -e

[[ $SOCKS_PROXY ]]

(( tries = 20 ))

while ! {
    curl -s --proxy "$SOCKS_PROXY" https://check.torproject.org | \
    grep Congratulations
} && (( --tries )); do
    sleep 1
    echo "$tries tries remaining"
done
(( tries ))
