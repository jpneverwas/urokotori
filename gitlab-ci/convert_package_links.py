"""Derive public download link from cryptic GitLab resource objects."""
import os
import re
import sys
import urllib.request
import urllib.parse
import asyncio
import json

from io import BytesIO
from unittest.mock import Mock
from http.client import HTTPException, HTTPResponse
from http import HTTPStatus

from distutils.version import LooseVersion

from typing import (
    List, Dict, Any, Tuple, Generator, Awaitable, Iterator, Optional, Callable,
)

CI_PROJECT_URL = (
    os.getenv("CI_PROJECT_URL") or "https://gitlab.com/jpneverwas/urokotori"
)
CI_PROJECT_ID = os.getenv("CI_PROJECT_ID") or "23665555"
CI_API_V4_URL = os.getenv("CI_API_V4_URL") or "https://gitlab.com/api/v4"
CI_JOB_TOKEN = os.getenv("CI_JOB_TOKEN")

headers = (
    {"JOB-TOKEN": CI_JOB_TOKEN} if CI_JOB_TOKEN
    else {"Private-Token": os.getenv("PRIVATE_TOKEN") or "FAKE"}
)
base_url = f"{CI_API_V4_URL}/projects/{CI_PROJECT_ID}/packages"

wanted = ("links.tar.gz",  "maildir.tar.gz", "mbox.tar.gz")

relpat = re.compile('.*<(http[^>]+)>; rel="next".*')


def _check_status(line) -> None:
    version, *rest = line.split()
    assert version.startswith(b"HTTP/")
    rest.reverse()
    status = int(rest.pop())
    if not 200 <= status < 300:
        raise HTTPException(HTTPStatus(status))


def _read_next_chunk_size(line: bytes) -> int:
    i = line.find(b";")
    if i >= 0:
        line = line[:i]
    return int(line, 16)


async def _make_async_request(full_url: str) -> HTTPResponse:
    url = urllib.parse.urlsplit(full_url)
    port = url.port
    if not port:
        port = 443 if url.scheme == "https" else 80
    reader, writer = await asyncio.open_connection(
        url.hostname, port, ssl=(url.scheme == "https" or None)
    )
    path = url.path or "/"
    header_lines = (
        f"GET {path} HTTP/1.1",
        f"Host: {url.hostname}",
        *("{k}: {v}" for k, v in headers.items()),
        "",
        "",
    )
    query = "\r\n".join(header_lines)
    writer.write(query.encode("latin-1"))

    raw_response = bytearray(await reader.readline())
    bod = False
    chunked = False

    _check_status(raw_response)

    async for line in reader:
        if not bod and not line.strip():
            if b"chunked" in raw_response.lower():  # TIL bytearray.lower!
                chunked = True
            else:
                writer.close()
            bod = True
        raw_response.extend(line)
        if chunked and line.strip():
            size = _read_next_chunk_size(line)
            if not size:
                break
            raw_response.extend(await reader.readexactly(size))

    writer.close()
    sock = Mock()
    sock.makefile = lambda _: BytesIO(raw_response)
    resp = HTTPResponse(sock)
    resp.begin()
    return resp


def _make_request(url: str) -> List[Dict[str, Any]]:
    req = urllib.request.Request(url, headers=headers)
    m = None
    with urllib.request.urlopen(req) as response:
        rels = response.headers.get("Link")
        m = relpat.match(rels)
        out = json.load(response)
        assert isinstance(out, list)
    if not m:
        return out
    return out + _make_request(m.group(1))


# Could just use delete_api_path but meh (._links may be "private"?)
def get_package_ids(branch: str) -> Generator[Tuple[str, str], None, None]:
    params = {
        "package_name": branch,
        "order_by": "version",  # broken as of 01/2021
        "per_page": 20,  # assuming avg is 200
    }
    url = f"{base_url}?{urllib.parse.urlencode(params)}"
    obj = _make_request(url)
    return ((o["version"], o["id"]) for o in obj)


async def get_file_ids(
    package_id: str
) -> Generator[Dict[str, str], None, None]:
    url = f"{base_url}/{package_id}/package_files"
    resp = await _make_async_request(url)
    obj: List[Dict[str, Any]] = json.load(resp)

    # Remove dupes (favor more recent uploads)
    objs = {
        o["file_name"]: o["id"] for o in obj
        if o["file_name"] in wanted
    }

    return (
        {"id": idee, "file_name": name}
        for name, idee in sorted(objs.items())
    )


def format_line(
    idee: str, branch: str, version: LooseVersion, name: str
) -> str:
    template = '<li><a href="{href}">{text}</a></li>'
    return template.format(
        href=f"{CI_PROJECT_URL}/-/package_files/{idee}/download",
        text=f"{branch}-{version}-{name}"
    )


# If you pass this func's return value to as_completed(), it'll reject it,
# saying "expect an iterable of futures, not generator." This happens because
# asyncio.coroutines.iscoroutine() validates it as a coro. (BTW, wrapping
# members in ensure_future() doesn't do anything.) Much like using Awaitable[T]
# instead of Coroutine[None, None, T], you can use Iterator[T] or Iterable[T]
# instead of Generator[T, None, None]. Perhaps this should be used to prevent
# such confusion by delcaring that the func returns a "degenerate" generator.
def prep_file_id_info(
    branch
) -> Iterator[Awaitable[Tuple[LooseVersion, Iterator[Dict[str, str]]]]]:
    async def _get_file_info(
        version: str, pkg_id: str
    ) -> Tuple[LooseVersion, Iterator[Dict[str, str]]]:
        return LooseVersion(version), await get_file_ids(pkg_id)

    return (
        _get_file_info(version, pkg_id)
        for version, pkg_id in get_package_ids(branch)
    )


async def visit_info(
    branch: str,
    info_cb: Callable[[Dict[str, str]], Any],
    version_cb: Optional[Callable[[LooseVersion], None]] = None,
) -> None:
    for coro in asyncio.as_completed((*prep_file_id_info(branch),)):
        version, file_info = await coro
        if version_cb:
            version_cb(version)
        for info in file_info:
            if (rv := info_cb(info)) is not None:
                return rv
    return None


async def inspect_info(branch: str) -> None:
    print("branch:", branch)
    await visit_info(
        branch,
        lambda info: print(f"{info=}"),
        lambda version: print("version:", version)
    )
    return None


async def find_most_recent_version_without_mbox(branch: str):
    file_id_info = await asyncio.gather(*prep_file_id_info(branch))

    for version, file_info in reversed(sorted(file_id_info)):
        if len(list(file_info)) == 2:
            print(version)
            break


async def get_download_urls(branch: str) -> None:
    file_id_info = await asyncio.gather(*prep_file_id_info(branch))

    for version, file_info in reversed(sorted(file_id_info)):
        for info in file_info:
            idee = info["id"]
            name = info["file_name"]
            print(format_line(idee, branch, version, name))
    return None


def main():
    branch = len(sys.argv) and sys.argv[1]
    assert branch
    asyncio.run(get_download_urls(branch))
    sys.stdout.flush()


if __name__ == "__main__":
    main()
