#!/bin/bash

set -e
thisfile=$(realpath "${BASH_SOURCE[0]}")
thisdir=${thisfile%/*}

[[ -f $thisdir/helpers.bash ]]
#                                                     shellcheck disable=SC1091
source "$thisdir/helpers.bash"

ID_SUBDIR=id
: "${OUTPUT_DIR:=public}"

: "${BASE_API_URL:=$CI_API_V4_URL/projects/$CI_PROJECT_ID}"
PKG_URL=$BASE_API_URL/packages/generic

declare LATEST

# Max inodes allowed for demo
: "${MAX_IDS:=195000}"

declare -a branches
declare -i max_branch_ids

populate_branches() {
    local line
    while read -r line; do
        line=${line#* }
        line=${line#*/}
        [[ $line == "$CI_DEFAULT_BRANCH"* ]] && continue
        [[ $line == *"HEAD detached"* ]] && continue
        branches+=( "$line" )
    done < <(git branch -r --no-color)
    max_branch_ids=$(( MAX_IDS / ${#branches[@]} / 2 ))
}

populate_versions() {
    (( $# == 2 ))
    local -n out
    out=$1
    branch=$2
    local cmdline
    cmdline=( bash "$thisdir/get_versions.sh" "$CI_PROJECT_PATH" "$branch" )
    #                                                 shellcheck disable=SC2034
    readarray -t out < <( "${cmdline[@]}" | cut -d" " -f1 | sort -V )
}

get_last_version() {
    local -a versions
    local branch
    [[ $1 ]]
    populate_versions versions "$1"
    if (( ! ${#versions[@]} )); then
        echo "No versions found for $1"
        return 1
    fi
    printf '%s\n' "${versions[-1]}"
}

collect_all() {
    local branch v_url r_url latest _current
    local -a versions
    branch=$1
    populate_versions versions "$branch"
    mkdir -p msg_ids
    # If there's no version matching the latest release, assume we're caught up
    r_url="$BASE_API_URL/jobs/artifacts/$branch/raw/links.tar.gz?job=release"
    curl -sSf -H "Job-Token: $CI_JOB_TOKEN" -L -o links.tar.gz "$r_url"
    latest=$(tar -xvf links.tar.gz | tee /dev/stderr)
    [[ $latest == *.csv ]]
    for ver in "${versions[@]}"; do
        is_version "$ver"
        v_url=$PKG_URL/$branch/$ver
        curl -sSf -H "Job-Token: $CI_JOB_TOKEN" -LO "$v_url/links.tar.gz"
        _current=$(tar -xvf links.tar.gz | tee /dev/stderr)
        [[ $latest && $latest == "$_current" ]] && latest=
        rm -f links.tar.gz
    done
    [[ $latest ]] && LATEST=${latest%*.csv}
    mv -f ./*.csv msg_ids/
    find msg_ids -mindepth 1 | wc -l
}

fetch_latest() {
    local url
    [[ $1 ]]
    [[ $PKG_URL == https://* ]]
    collect_all "$1"
}

process_entry() {
    local line url msg_id _leaf slug _penult period safe
    local -n out
    out=$1
    line=$2
    url=${line%,*}         # http://lists.gnu.org/archives/html/proj/2020-01.html
    #
    msg_id=${line#*,}
    _leaf=${url##*/}       # 2020-01.html
    slug=${_leaf%.*}       # 2020-01
    _penult=${url%/*}      # http://lists.gnu.org/archives/html/bash
    period=${_penult##*/}  # bash
    safe=${msg_id//\//_}
    #                                                 shellcheck disable=SC2034
    out=([url]=$url [period]=$period [msg_id]=$msg_id [safe]=$safe [slug]=$slug)
}

process_branch() {
    local branch line csvz
    local -i count
    branch=$1
    get_last_version "$branch" || return 0
    LATEST=
    fetch_latest "$branch"
    readarray -t csvz < <(find msg_ids -mindepth 1 | sort -r)
    while (( count++ < max_branch_ids )) && read -r line; do
        local -A parts
        process_entry parts "$line"
        mkdir -p "$branch/$ID_SUBDIR"
        # Skip when file name too long
        if printf %s "${parts[url]}" > "$branch/$ID_SUBDIR/${parts[safe]}"
        then
            mkdir -p "$branch/${parts[period]}"
            printf %s "${parts[msg_id]}" > "$branch/${parts[period]}/${parts[slug]}"
        fi
        unset parts
    done < <(sort -r "${csvz[@]}")  # too wasteful to sort everything first?
    rm -rf ./msg_ids
}

print_latest() {
    local branch arturl version
    branch=$1
    arturl=$BASE_API_URL/jobs/artifacts
    is_period "$LATEST"
    version=$(to_version "$branch-$LATEST")
    printf '<li><a href="%s">%s</a></li>\n' \
            "$arturl/$branch/raw/links.tar.gz?job=release" \
            "$branch-$version-links.tar.gz" \
            "$arturl/$branch/raw/maildir.tar.gz?job=release" \
            "$branch-$version-maildir.tar.gz" \
            "$arturl/$branch/raw/mbox.tar.gz?job=release" \
            "$branch-$version-mbox.tar.gz"
}

main() {
    [[ $(realpath .) == "$(realpath "$CI_PROJECT_DIR")" ]]
    mkdir -p "$OUTPUT_DIR"
    pushd $OUTPUT_DIR

    populate_branches
    local branch

    for branch in "${branches[@]}"; do
        echo "Processing branch: $branch"
        process_branch "$branch"
        (
            [[ $LATEST ]] && print_latest "$branch"
            python "$thisdir/convert_package_links.py" "$branch"
        ) > "$branch/package_links"
    done
    popd
}

test_process_entry() {
    local msg_id url
    local -A parts
    url="https://lists.gnu.org/archive/html/mailman/2004-05/msg01234.html"
    msg_id="20040528165527.TN12394@znvyzna.tah.bet"
    process_entry parts "$url,$msg_id"
    [[ ${parts[msg_id]} == "$msg_id" ]] || return 1
    [[ ${parts[url]} == "$url" ]] || return 2
    [[ ${parts[period]} == "2004-05" ]] || return 3
    [[ ${parts[slug]} == "msg01234" ]] || return 4
    echo .test_process_entry
}

test_process_branch() {
    local tmpdir orig_get_last_version orig_fetch_latest
    orig_get_last_version=$(declare -f get_last_version)
    orig_fetch_latest=$(declare -f fetch_latest)
    tmpdir=$(mktemp -p /tmp -d tmp.urokotori.deploy.XXXXXXXXXX)
    pushd "$tmpdir" >/dev/null
    get_last_version() { return 0; }
    fetch_latest() {
        mkdir msg_ids
        cat > msg_ids/2020-01.csv <<'EOF'
https://lists.gnu.org/archive/html/fake/2020-01/msg00001.html,dead1234
https://lists.gnu.org/archive/html/fake/2020-01/msg00004.html,ba@xx.xx
https://lists.gnu.org/archive/html/fake/2020-01/msg00002.html,beef1234
https://lists.gnu.org/archive/html/fake/2020-01/msg00005.html,baz12345
https://lists.gnu.org/archive/html/fake/2020-01/msg00003.html,a/b@c.xy
EOF
        cat > msg_ids/2019-12.csv <<'EOF'
https://lists.gnu.org/archive/html/fake/2019-12/msg00003.html,foo99999
https://lists.gnu.org/archive/html/fake/2019-12/msg00001.html,dead9876
https://lists.gnu.org/archive/html/fake/2019-12/msg00002.html,beef9876
EOF
    }
    max_branch_ids=7
    _assert_0 process_branch fake
    local expect
    expect=https://lists.gnu.org/archive/html/fake/2020-01/msg00004.html
    [[ $(< fake/id/ba@xx.xx) ==  "$expect" ]] ||
        return 2
    expect=https://lists.gnu.org/archive/html/fake/2020-01/msg00003.html
    [[ $(< fake/id/a_b@c.xy) ==  "$expect" ]] ||
        return 3
    expect=https://lists.gnu.org/archive/html/fake/2019-12/msg00003.html
    [[ $(< fake/id/foo99999) ==  "$expect" ]] ||
        return 4
    [[ $(< fake/2020-01/msg00003) ==  "a/b@c.xy" ]] || return 5
    # msg00001 is never written out
    expect=$'msg00002\nmsg00003'
    [[ $(cd "$tmpdir/fake/2019-12"; find . -mindepth 1 -printf '%f\n') == "$expect" ]] ||
        return 6
    popd >/dev/null
    eval "$orig_get_last_version"
    eval "$orig_fetch_latest"
    echo .test_process_branch
}

"$@"
