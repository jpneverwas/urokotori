#!/bin/bash

set -e

readarray -t all_indexes < <(find public -name index.html)
[[ -f public/index.html ]]

# Inject some CSS into index.html files at any depth
twbscss=https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css
cmd=$'/<\/head>/i\<link href="'"$twbscss"$'" rel="stylesheet" crossorigin="anonymous">\
<meta name="viewport" content="width=device-width, initial-scale=1">\
<style>\
ul > li:nth-child(3n) > a {color: #664d03 !important;}\
ul > li:nth-child(3n+1) > a {color: #ca6510 !important;}\
ul > li:nth-child(3n+2) > a {color: #cc9a06 !important;}\
ul > li > a:hover {color: #6610f2 !important; background-color: #efdd39;}\
</style>'

sed -i -e "$cmd" -e 's/<body>/<body class="pt-5 container">/' "${all_indexes[@]}"

# Replace index links

for dex in public/*/index.html; do
    awk '/<[!]/,/<ul>/ {next}; /<li>/ {next}; /./' "$dex" > .aft
    awk '/<li>/ {next}; /<\/ul>/,/<\/html>/ {next}; /./' "$dex" > .bef
    cat .bef "${dex%/*}/package_links" .aft > "$dex"
    rm -f .bef .aft "${dex%/*}/package_links"
done

# Add example to root index.html
example=
while read -r line ; do
    example+=${line/<pre>/<pre class=\"pb-3\">}$'\n'
done < <(
pygmentize -f html -l console <<'EOF'
>$ curl https://jpneverwas.gitlab.io/urokotori/myproj/id/h%21_w%40r.ld
https://lists.gnu.org/archive/html/myproj/2001-05/msg00042.html
>$ curl https://jpneverwas.gitlab.io/urokotori/myproj/2001-05/msg00042
h!/w@r.ld
EOF
)

# Scrape existing projects
readarray -t projects < <(find public -mindepth 1 -maxdepth 1 -type d -printf '%f\n')

paint_project_line() {
    local proj pds
    pushd public >/dev/null
    for proj in "${projects[@]}"; do
        pushd "$proj" >/dev/null
        readarray -t pds \
            < <(find . -mindepth 1 -maxdepth 1 -type d ! -name id -printf '%f\n' | sort)
        cat <<EOF
      <tr>
       <td><a class="se nv-hov" href="$proj/">$proj/</a></td>
       <td><span class="badge text-secondary bg-light ">\
$(find "${pds[0]}" -mindepth 1 ! -name index.html | sort | head -n1)</span></td>
       <td><span class="badge text-secondary bg-light ">\
$(find "${pds[-1]}" -mindepth 1 ! -name index.html | sort | tail -n1)</span></td>
      </tr>
EOF
        popd >/dev/null
    done
    popd >/dev/null
}

# Render
style=$(pygmentize -f html -S autumn | tail -n+2 | sed 's/^\.sb/.gp/')
ghribcss=https://cdnjs.cloudflare.com/ajax/libs/github-fork-ribbon-css/0.2.3/gh-fork-ribbon.min.css

cat > public/index.html <<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Urokotori</title>
  <link href="$twbscss" rel="stylesheet">
  <link href="$ghribcss" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
  .bg-danger-cust {background: #bb4e43}
  .text-primary-cust {color: #bb4e43}
  .table > :not(:last-child) > :last-child > th.border-cust {border-bottom-color: #dee2e6;}
  .table > :last-child > :not(:last-child) > td {border: none;}
  .nv-hov:hover {color: #aa0000;}
  $style
  </style>
 </head>
 <body class="pt-5 container">
   <a class="github-fork-ribbon" href="https://gitlab.com/jpneverwas/urokotori"
      data-ribbon="Yeah, it's GitLab (sigh)"
      title="Fork me on GitLab">Fork me on GitLab</a>
  <div class="row row-cols-auto">
   <div class="col">
    <div class="d-flex flex-column flex-md-row align-items-baseline">
     <span class="display-3">Demo service</span>
     <small class="ms-3 text-secondary">(capped at 50k msgs/group<sup>‡</sup>)</small>
    </div>
   </div>
  </div>
  <div class="mt-4 row row-cols-auto">
   <div class="col">
    <h2>Message IDs</h2>
    <p>Example <small class="text-secondary">
    (must <code>tr / _</code> for now, sadly)</small>
    </p>
    $example
    </div>
  </div>
  <div class="row row-cols-auto">
   <div class="col">
    <h2>Maildirs</h2>
    <p>Messages include an extra header like</p>
    <pre class="text-muted">
    X-Gnu-Archive: https://lists.gnu.org/archive/html/myproj/2001-05/msg00042.html
    </pre>
   </div>
  </div>
  <div class="row row-cols-auto">
   <div class="col">
    <h2>Resources</h2>
    <table class="table">
     <thead class="text-secondary fst-normal">
      <tr>
       <th class="border-cust fw-normal" scope="col">Project</th>
       <th class="border-cust fw-normal" scope="col">Oldest<sup>‡</sup></th>
       <th class="border-cust fw-normal" scope="col">Latest</th>
      </tr>
     </thead>
      <tbody>
$(paint_project_line)
      </tbody>
    </table>
   </div>
  </div>
   <p><small class="text-secondary"><sup>‡</sup>
   Actually, the limit is ~100k messages for the demo service.  But tarballs
   are available all the way back.</small>
   </p>
  <footer class="mt-5 mb-3 card border-0">
   <div class="row g-0">
    <div class="col-md-2 d-flex flex-column justify-content-center
                align-items-center bg-danger-cust">
     <p class="mt-2 display-4 text-white">Oi!</p>
    </div>
    <div class="col-md-10">
     <div class="col card-body">
      <p class="card-text">Got some spare compute lying around?</p>
      <p class="card-text"><small class="text-secondary">
      It'd be nice to have a dynamic endpoint with some actual smarts, so we
      could <code>PUT</code> to update, etc., instead of this hobbled
      nonsense. Just a thought.</small>
      </p>
     </div>
    </div>
   </div>
  </footer>
 </body>
</html>
EOF
