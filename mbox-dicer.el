;;; mbox-dicer.el --- mbox to one-file-per-message -*- lexical-binding: t; -*-

;;; Commentary:

;; WARNING: only run this in a sandbox.
;;
;; This to help non-Gnus users break up mboxes into individual files. By
;; default it produces a maildir tree. Pass BACKEND=nnml (or nnmh) if you
;; prefer those formats. They consume fewer inodes but are less portable
;; according to (info "(gnus) Choosing a Mail Back End"). Export env var
;; MSG_DIR=/some/dir to override the default root location for either method.
;;
;; A source directory containing archive mboxes should be exported as env var
;; INBOX.
;;
;; WARNING: anything in this directory will be clobbered!
;;
;; To add an extra header with an updated link to a GNU lists archive web
;; page, add a file like 2000-09.csv alongside every mbox 2000-09 in the
;; source directory. See elsewhere in this repo for the format of the CSVs.
;;
;; In the case of duplicates, this may map multiple emails containing the same
;; message id to a single html page. For example, the March 2005 archive for
;; emacs help has two occurrences of jwv3bv3saok.fsf-monnier+inbox@gnu.org,
;; along with five other dupes. However, it's definitely an error if the set
;; of message ids is not "totally" consumed. BTW, a cursory inspection of
;; random samples suggests these dupes are mostly spam.
;;
;; TODO add some kind of sanity check here instead of relying on Makefile

;;; Code:

(require 'cl-lib)

(require 'gnus)
(require 'gnus-sum)
(require 'gnus-group)

(require 'trace)

(defvar mbox-dicer-header-name "X-Gnu-Archive")
(defvar mbox-dicer-inbox (file-name-as-directory (getenv "INBOX")))
(defvar mbox-dicer-backend (if-let ((v (getenv "BACKEND")))
                               (intern v)
                             'nnmaildir))
(defvar mbox-dicer-backup (getenv "BACKUP")) ; also export mbox

(defvar mbox-dicer-group (getenv "GROUP"))
(defvar mbox-dicer-msg-dir (getenv "MSG_DIR"))

(defvar mbox-dicer-method nil)
(defvar mbox-dicer-links nil) ; hash table: id (no <brackets>) -> link

(defvar mbox-dicer-added 0)
(defvar mbox-dicer-seen 0)

(defvar mbox-dicer--method-name "lists") ; not important ("nnml:" looks funny)

(defvar mbox-dicer--debug-seen nil)
(defvar mbox-dicer--debug-msg-dir (getenv "DEBUG_MSG_DIR"))

(defvar mbox-dicer-pat
  "\\([[:digit:]]\\{4\\}-[[:digit:]]\\{2\\}\\)$")


(defun mbox-dicer--resolve-method ()
  (pcase mbox-dicer-backend
    ;; According to (info "(gnus) Maildir") directory is required
    ('nnmaildir `(nnmaildir ,mbox-dicer--method-name
                            (get-new-mail t)
                            (directory ,(or (expand-file-name mbox-dicer-msg-dir)
                                            "~/Maildir"))))
    ((or 'nnml 'nnmh)
     (append (list mbox-dicer-backend mbox-dicer--method-name
                   (when mbox-dicer-msg-dir
                     (list (intern (format "%s-directory" mbox-dicer-backend))
                           (expand-file-name mbox-dicer-msg-dir))))))
    (_ (error "Unsupported backend: %S" mbox-dicer-method))))

(defun mbox-dicer--add-link (id link)
  (let* ((quotedp (and (eq  ?\" (aref id 0))
                       (eq  ?\" (aref id (1- (length id))))))
         (key (if quotedp
                  (replace-regexp-in-string "\"\"" "\""
                                            (substring  id 1 -1)
                                            'fixedcase
                                            'literal)
                id)))
    (puthash key link mbox-dicer-links)))

(defun mbox-dicer-add-links (name)
  (unless mbox-dicer-links
    (setq mbox-dicer-links (make-hash-table :test #'equal)))
  (with-temp-buffer
    (insert-file-contents-literally name)
    (goto-char (point-min))
    (while (and (looking-at "^\\([^,]+\\),\\(.+\\)$")
                (let ((id (match-string 2))
                      (link (match-string 1)))
                  (mbox-dicer--add-link id link))
                (zerop (forward-line)))))
  (message "links: %s" (hash-table-count mbox-dicer-links)))

(defun mbox-dicer--debug-msg-dir-save (&optional tag)
  (let* ((content (buffer-string))
         (file-name (concat tag (format-time-string "%H:%M:%S.%N")
                            (format "%s" (sxhash-eq content)))))
    (with-temp-file (expand-file-name file-name
                                      mbox-dicer--debug-msg-dir)
      (insert content))))

(defun mbox-dicer--inject-header-insert (message-id)
  (when mbox-dicer--debug-msg-dir
    (when (member message-id mbox-dicer--debug-seen)
      (mbox-dicer--debug-msg-dir-save))
    (push message-id mbox-dicer--debug-seen))
  (goto-char (point-max))
  (skip-syntax-backward " ")
  (goto-char (line-beginning-position))
  (insert (format "%s: %s\n" mbox-dicer-header-name message-id))
  (cl-incf mbox-dicer-added))

;; Fallback case is to loop back over all the keys and try for an exact match.
;; Alternatively, we could stash outliers while discarding used message-ids.
;; But this would require a second data point to match against, meaning we'd
;; need to revisit the crawler and extract another field (probably date).
(defun mbox-dicer-inject-header ()
  (let ((field-value (message-fetch-field "Message-Id")))
    (if-let* ((id (and field-value
                       ;; (string-match-p "@\\|%40" field-value)
                       (string-match "[<]\\([^>]+\\)[>]" field-value)
                       (match-string 1 field-value)))
              (found (or (gethash id mbox-dicer-links)
                         (gethash (setq id (string-trim id))
                                  mbox-dicer-links))))
        (progn
          (remhash id mbox-dicer-links)
          (mbox-dicer--inject-header-insert found))
      ;; Honor 1:1 matches that don't adhere to conventional form
      (if-let ((found (and field-value
                           (gethash field-value mbox-dicer-links))))
          (progn
            (remhash field-value mbox-dicer-links)
            (mbox-dicer--inject-header-insert found))
        (when mbox-dicer--debug-msg-dir
          (mbox-dicer--debug-msg-dir-save "stray-"))
        (let ((orig (message-fetch-field "Original-Message-Id")))
          ;; Gmane or gnus turns bad message IDs like "PM20008:37:25 AM" into
          ;; something like "<k0e6vyyx.fsf@totally-fudged-out-message-id>"
          (when orig
            (remhash orig mbox-dicer-links))
          (message "Found a stray: {id: %s, originally: %s}\n%s"
                   field-value (or orig "(same)") (buffer-string)))))))

(defun mbox-dicer-on-nnmail-prepare-incoming-message ()
  ;; Sort of a shame since caller knows message ID; oh well
  (when mbox-dicer-links
    (cl-incf mbox-dicer-seen)
    (mbox-dicer-inject-header)))

(defun mbox-dicer-debug-source-predicate (p)
  (when (string-match-p mbox-dicer-pat p)
    (let ((csv (concat p ".csv")))
      (when (file-exists-p csv)
        (mbox-dicer-add-links csv)
        (gnus-delete-file csv)))
    t))

(defun mbox-dicer-setup ()
  (interactive)
  (setq mbox-dicer-method (mbox-dicer--resolve-method))
  (message "method: %s" mbox-dicer-method)
  (add-hook 'nnmail-prepare-incoming-header-hook
            #'mbox-dicer-on-nnmail-prepare-incoming-message)
  (setq message-directory (expand-file-name "Mail")
        gnus-select-method '(nnnil)
        gnus-dribble-directory default-directory
        gnus-secondary-select-methods `(,mbox-dicer-method)
        gnus-startup-file (expand-file-name ".newsrc")
        mail-source-delete-incoming t
        mail-source-crash-box (expand-file-name ".emacs-mail-crash-box")
        nnmail-resplit-incoming t
        nnmail-cache-accepted-message-ids t
        nnmail-treat-duplicates 'delete
        nnmail-message-id-cache-length 10000
        nnmail-message-id-cache-file (expand-file-name ".nnmail-cache")
        nnmail-split-methods `((,mbox-dicer-group ""))
        mail-sources `((directory :path ,mbox-dicer-inbox
                                  :suffix ""
                                  :predicate #'mbox-dicer-debug-source-predicate)))
  (unless (getenv "SAVEDIR")
    (setq gnus-directory (expand-file-name "News"))))

(defun mbox-dicer-ensure-group ()
  (let ((nname (gnus-group-prefixed-name mbox-dicer-group mbox-dicer-method)))
    (if (gnus-group-entry nname)
        (message "Found existing group: %s" nname)
      (condition-case err
          (gnus-group-make-group mbox-dicer-group mbox-dicer-method)
        ;;
        ;; Could instead do something like this in main():
        ;;
        ;; | (when (eq mbox-dicer-backend 'nnmaildir)
        ;; |   (gnus-fetch-group "nndraft:drafts")
        ;; |   (mbox-dicer-ensure-group)
        ;; |   (gnus-group-exit))
        ;;
        (error (if (and (eq mbox-dicer-backend 'nnmaildir)
                        (string-prefix-p "Could not create group" (cadr err)))
                   (progn (let ((gnus-read-active-file t)
                                (gnus-agent gnus-plugged))
                            (gnus-read-active-file))
                          (gnus-group-unsubscribe-group nname))
                 (signal (car err) (cdr err))))))))

(defun mbox-dicer--reckon ()
  (gnus-group-quit)
  (message "Seen: %d, added: %d" mbox-dicer-seen mbox-dicer-added)
  (message "%s" (with-current-buffer trace-buffer (buffer-string)))
  (cl-assert mbox-dicer-links)
  (cl-assert (>= mbox-dicer-seen mbox-dicer-added))
  (kill-emacs (cond ((= mbox-dicer-seen mbox-dicer-added)
                     0)
                    ((map-empty-p mbox-dicer-links)
                     0)
                    ((= (hash-table-count mbox-dicer-links)
                        (- mbox-dicer-seen mbox-dicer-added))
                     (message "ignored: %s" (map-keys mbox-dicer-links))
                     0)
                    ((progn
                       (dolist (k (map-keys mbox-dicer-links))
                         (when (string-match-p "NO-ID-FOUND" k)
                           (message "ignored: %s" k)
                           (remhash k mbox-dicer-links)))
                       (map-empty-p mbox-dicer-links))
                     0)
                    ((getenv "ALLOW_UNHANDLED")
                     (message "unhandled: %s" (map-keys mbox-dicer-links))
                     0)
                    (t
                     (message "unhandled: %s" (map-keys mbox-dicer-links))
                     1))))

(defun mbox-dicer-copy-one (method)
  (save-excursion
    (let ((name (gnus-group-prefixed-name mbox-dicer-group mbox-dicer-method))
          gnus-large-newsgroup
          gnus-show-threads
          gnus-preserve-marks)
      (gnus-group-quick-select-group t name)
      (cl-assert (eq major-mode 'gnus-summary-mode))
      (gnus-uu-mark-buffer)
      (gnus-summary-copy-article nil nil method)
      (gnus-summary-exit))))

(defun mbox-dicer-main()
  (cl-assert noninteractive)
  (trace-function-background #'nnmail-split-incoming)
  (mbox-dicer-setup)
  (gnus)
  (mbox-dicer-ensure-group)
  (gnus-group-get-new-news)
  (when mbox-dicer-backup
    (mbox-dicer-copy-one '(nnmbox "backup"
                                  (nnmbox-mbox-file "mbox")
                                  (nnmbox-active-file ".mbox-active"))))
  (mbox-dicer--reckon))

;;; mbox-dicer ends here
