module gitlab.com/jpneverwas/urokotori

go 1.17

require (
	github.com/antchfx/antch v0.0.0-20200531151208-b3a15a669131
	github.com/antchfx/htmlquery v1.2.3
)

require (
	github.com/antchfx/jsonquery v1.1.4 // indirect
	github.com/antchfx/xmlquery v1.3.6 // indirect
	github.com/antchfx/xpath v1.1.10 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/temoto/robotstxt v1.1.2 // indirect
	golang.org/x/net v0.0.0-20200813134508-3edf25e44fcc // indirect
	golang.org/x/text v0.3.0 // indirect
)
