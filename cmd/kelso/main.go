// Package kelso extracts message-ids from mailing-list archives created with
// MHonArc: https://github.com/Conservatory/MHonArc
//
// It wants the URL of an article-summary page displaying a range of posts,
// perhaps over some period. It can also handle lists.gnu.org archive index
// pages, like https://lists.gnu.org/archive/html/help-gnu-emacs/, which list
// summary pages by period. Other sites, like https://www.spinics.net/lists,
// may not have indexes (although spinics does provide adware).
//
// By default, this spits out a comma-separated list of message-id/URL pairs
// to stdout. If either contains commas or newlines, that would be bad.
//
package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"os/signal"
	"runtime"

	"strings"
	"syscall"
	"time"

	"github.com/antchfx/antch"
	"gitlab.com/jpneverwas/urokotori/pkg/mhonarchy"
)

var idleTimeout = 60 * time.Second
var maxTries = 3
var flagPages string
var flagPosts string
var flagIndex string

func usage() {
	fmt.Fprintf(
		flag.CommandLine.Output(),
		"Usage of %s:\n" +
			"\n  Target-selection options are mutually exclusive.\n\n",
		os.Args[0],
	)
	flag.PrintDefaults()
}

// All options are mutually exclusive
func initOpts() {
	if flagPages + flagPosts + flagIndex != "" {
		return
	}
	flag.StringVar(
		&flagIndex, "index", "", "A single lists.gnu.org/archive/html/x URL",
	)
	flag.StringVar(
		&flagPages, "pages", "", "Whitespace separated summary/thread URLs",
	)
	flag.StringVar(
		&flagPosts, "posts", "",
		"Whitespace separated article URLs of individual posts",
	)
	flag.Usage = usage
	flag.Parse()
}

// Instead of adding handlers for posts, we could just use the common prefix
// of /msg* so long as we don't need to pass special info to each spider
func initCrawler(crawler *antch.Crawler) []string {
	var startURLs []string
	var parts []string
	var spider func() antch.Handler
	if flagIndex != "" {
		parts = strings.Fields(flagIndex)
		spider = mhonarchy.CreateDefaultIndexSpider
	} else if flagPages != "" {
		parts = strings.Fields(flagPages)
		spider = mhonarchy.CreateDefaultThreadSpider
	} else {
		parts = strings.Fields(flagPosts)
		spider = mhonarchy.CreateDefaultPostSpider
	}
	for _, raw := range parts {
		u, err := url.Parse(mhonarchy.EnsureSlash(raw))
		if err != nil {
			panic(err)
		}
		pat := u.Host + u.Path
		log.Println("Registering:", pat)
		crawler.Handle(pat, spider())
		startURLs = append(startURLs, u.String())
	}
	return startURLs
}

var heartbeat = make(chan int)
var scheduler = make(chan *mhonarchy.LinkJob)
var csvOutput io.Writer = os.Stdout

func initPipelines() []antch.Pipeline {
	return []antch.Pipeline{
		mhonarchy.CreateFollowPipeline(heartbeat, scheduler),
		mhonarchy.CreateTrimOutputPipeline(),
		mhonarchy.CreateCSVPipeline(csvOutput),
	}
}

// No actual reason for using NumCPU here. See throttle() below
var maxEnqueued = runtime.NumCPU()
var downloadDelay = 250 * time.Millisecond
var closer chan struct{} = make(chan struct{})
var cLogger antch.Logger
var teardown func()

func main() {
	initOpts()
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	crawler := &antch.Crawler{
		Exit: closer, DownloadDelay: downloadDelay, ErrorLog: cLogger,
	}
	startURLs := initCrawler(crawler)
	crawler = crawler.UsePipeline(initPipelines()...)
	crawler, err := mhonarchy.MaybeInitSocks(crawler)
	if err != nil {
		log.Fatalf("%v", err)
	}
	idleTimer := time.NewTimer(idleTimeout)
	go crawler.StartURLs(startURLs)
	var enqueued = make(chan *mhonarchy.LinkJob, maxEnqueued)
	go mhonarchy.Throttle(crawler, enqueued)
	var processed, requested int
	for {
		select {
		case info := <-scheduler:
			ps, ok := info.Child.(*mhonarchy.PostSpider)
			if ! ok {
				log.Fatal("Bad Child")
			}
			if ps.Attempt == 0 {
				requested += 1
			} else if ps.Attempt == maxTries {
				log.Fatal("Max attempts exceeded:", info.URL.Path)
			}
			go func(lj *mhonarchy.LinkJob) {
				if ps.Attempt > 0 {
					time.Sleep(1000)
				}
				enqueued <- lj
			}(info)
		case incr := <-heartbeat:
			processed += incr
			idleTimer.Reset(idleTimeout)
		case <-idleTimer.C:
			goto exit
		// In case test decides to close
		case <-closer:
			goto exit
		case <-sigs:
			goto exit
		}
	}
exit:
	mhonarchy.RunTeardown()
	close(closer)
	if processed == requested {
		log.Printf("%d records processed\n", processed)
		return
	}
	if teardown != nil {
		teardown()
	}
	log.Fatalf("Mismatch: %d/%d", processed, requested)
}
