package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"testing"
	"time"

	helpers "gitlab.com/jpneverwas/urokotori/internal/testing"
)

var pageAddr = "http://127.0.0.1:18000/archive/html/qemu-devel/2020-12/"

type eLogger struct {
	t *testing.T
}

func (l eLogger) Output(maxdepth int, s string) error {
	closer <- struct{}{}
	l.t.Error(s)
	return nil
}

func TestWholeThing(t *testing.T) {
	dir := t.TempDir()
	teardown = helpers.StartServer(t, dir)
	t.Cleanup(func() {
		flagPages = ""
		teardown()
	})
	dstPath := filepath.Join(dir, "whole_thing.csv")
	dstFile, err := os.Create(dstPath)
	if err != nil {
		t.Error(err)
	}
	fmt.Println("Writing to temp file:", dstPath)
	r, w := io.Pipe()
	// Tweak some global values for test friendliness
	flagPages = pageAddr
	idleTimeout = 10 * time.Second
	downloadDelay = 10 * time.Millisecond
	csvOutput = w
	cLogger = eLogger{t}
	//
	var lineCt int
	go func() {
		// Wait till first widget ready
		heartbeat <- <- heartbeat
		s := bufio.NewScanner(r)
		for s.Scan() {
			fmt.Println(s.Text())
			dstFile.Write(s.Bytes())
			dstFile.Write([]byte{'\n'})
			lineCt++
		}
		if err := s.Err(); err != nil {
			t.Errorf("%v (lines: %d)", err, lineCt)
		}
	}()
	main()
	if lineCt != 6506 {
		t.Errorf("Bad line count: %d", lineCt)
	}
}
