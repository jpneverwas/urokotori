package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"github.com/antchfx/antch"
	"gitlab.com/jpneverwas/urokotori/pkg/mhonarchy"
)

var idleTimeout = 30 * time.Second
// Time to wait for most recent thread/date page to finish processing before
// starting next most recent concurrently.
var indexTimeout =  4*time.Second
var flagList string
var flagId string
var flagMax int
var baseListsURL = "https://lists.gnu.org/archive/html/"
var maxEnqueued = runtime.NumCPU()

// Using NumCPU here to provide arbitrary cap. See Throttle.
var stdout io.Writer = os.Stdout
var downloadDelay = 250 * time.Millisecond
var requestTimeout = 30 * time.Second

func initOpts() {
	if flagList + flagId != "" {
		return
	}
	flag.StringVar(&flagList, "list", "", "A GNU list, like help-gnu-emacs")
	flag.StringVar(&flagId, "id", "", "Message-Id to search for")
	flag.IntVar(&flagMax, "max", -1, "Limit search to this many articles")
	flag.Parse()
	if flagList == "" || flagId == "" {
		flag.Usage()
		os.Exit(0)
	}
}

type coms struct {
	heartbeat chan int
	scheduler chan *mhonarchy.LinkJob
	done chan string
	indexTimer *time.Timer
	closer chan struct{}
	crawler *antch.Crawler
	sigs chan os.Signal
	startURLs []string
}

func initComs() *coms {
	coms := &coms {
		heartbeat: make(chan int),
		scheduler: make(chan *mhonarchy.LinkJob),
		done: make(chan string),
		indexTimer: time.NewTimer(indexTimeout),
		closer: make(chan struct{}),
		sigs: make(chan os.Signal),
	}
	signal.Notify(coms.sigs, syscall.SIGINT, syscall.SIGTERM)
	return coms
}

func (c *coms)initPipelines() []antch.Pipeline {
	return []antch.Pipeline{
		mhonarchy.CreateFollowPipeline(c.heartbeat, c.scheduler),
		mhonarchy.CreateTrimOutputPipeline(),
		createCheckOutputPipeline(c.done, mhonarchy.Deburr(flagId), flagMax),
	}
}

func (c *coms)initCrawler() (error) {
	c.crawler = &antch.Crawler{
		Exit: c.closer,
		DownloadDelay: downloadDelay,
		RequestTimeout: requestTimeout,
	}
	u, err := url.Parse(baseListsURL + flagList + "/index.html")
	if err != nil {
		return err
	}
	pat := u.Host + u.Path
	log.Println("Registering:", pat)
	c.crawler.Handle(pat, mhonarchy.CreateDefaultIndexSpider())
	c.crawler = c.crawler.UsePipeline(c.initPipelines()...)
	c.crawler, err = mhonarchy.MaybeInitSocks(c.crawler)
	if err != nil {
		return err
	}
	c.startURLs = []string{mhonarchy.EnsureSlash(u.String())}
	return nil
}

func happen(coms *coms) string {
	err := coms.initCrawler()
	if err != nil {
		log.Fatalf("%v", err)
	}
	idleTimer := time.NewTimer(idleTimeout)
	go coms.crawler.StartURLs(coms.startURLs)
	var processed, requested int
	var found string
	tq := []*mhonarchy.LinkJob{}
	for {
		select {
		case <- coms.indexTimer.C:
			if len(tq) != 0 {
				info := tq[0]
				tq = tq[1:]
				mhonarchy.HandleOne(coms.crawler, info)
				info.Gate <- nil
			}
		case info := <-coms.scheduler:
			switch info.Child.(type) {
			case *mhonarchy.PostSpider:
				mhonarchy.HandleOne(coms.crawler, info)
				coms.indexTimer.Reset(indexTimeout)
				requested += 1
			case *mhonarchy.ThreadSpider:
				tq = append(tq, info)
			}
		case incr := <-coms.heartbeat:
			processed += incr
			idleTimer.Reset(idleTimeout)
		case <-idleTimer.C:
			goto exit
		case found = <-coms.done:
			goto exit
		// In case test decides to close
		case <-coms.closer:
			goto exit
		case <-coms.sigs:
			goto exit
		}
	}
exit:
	log.Printf("Processed %d of %d requested\n.", processed, requested)
	close(coms.closer)
	mhonarchy.RunTeardown()
	return found
}

func main() {
	initOpts()
	coms := initComs()
	if found := happen(coms); found != "" {
		fmt.Fprintln(stdout, found)
	}
}
