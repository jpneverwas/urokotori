package main

import (
	"log"

	"github.com/antchfx/antch"
	"gitlab.com/jpneverwas/urokotori/pkg/mhonarchy"
)

type checkOutputPipeline struct {
	collector chan string
	needle string
	next antch.PipelineHandler
	max int
	count int
}

func createCheckOutputPipeline(
	collector chan string, needle string, max int,
) antch.Pipeline {
	return func(next antch.PipelineHandler) antch.PipelineHandler {
		return &checkOutputPipeline{collector, needle, next, max, 0}
	}
}

func (p *checkOutputPipeline) ServePipeline(v antch.Item) {
	nug, ok := v.(*mhonarchy.Nugget)
	if !ok {
		log.Fatal("Wrong type, expected Nugget")
	}
	if p.needle == nug.Id {
		p.collector <- nug.Link
		return
	}
	p.count += 1
	if p.count == p.max {
		p.collector <- ""
	}
}
