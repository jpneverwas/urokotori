package main

import (
	"strings"
	"testing"
	"time"

	helpers "gitlab.com/jpneverwas/urokotori/internal/testing"
)


func TestNotFound(t *testing.T) {
	flagList = "qemu-devel"
	flagId = "fake"
	flagMax = 5
	baseListsURL = "http://127.0.0.1:18000/archive/html/"
	dir := t.TempDir()
	killer := helpers.StartServer(t, dir)
	// Tweak some global values for test friendliness
	idleTimeout = 10 * time.Second
	downloadDelay = 10 * time.Millisecond
	//
	var found string
	coms := initComs()
	go func() {found = happen(coms)}()
	// Wait till first widget ready
	coms.heartbeat <- <- coms.heartbeat
	<- coms.closer
	if found != "" {
		t.Errorf("Unexpected output: %s", string(found))
	}
	killer()
}

const expected = "http://127.0.0.1:18000/archive/html/qemu-devel/2020-12/msg06503.html"

func TestFound(t *testing.T) {
	flagList = "qemu-devel"
	flagId = "20201231224911.1467352-18-f4bug@amsat.org"
	flagMax = 200
	baseListsURL = "http://127.0.0.1:18000/archive/html/"
	dir := t.TempDir()
	killer := helpers.StartServer(t, dir)
	// Tweak some global values for test friendliness
	idleTimeout = 10 * time.Second
	downloadDelay = 10 * time.Millisecond
	//
	var found string
	coms := initComs()
	go func() {found = happen(coms)}()
	//
	coms.heartbeat <- <- coms.heartbeat
	<- coms.closer
	if found == "" {
		t.Error("Missing output")
	}
	if strings.TrimSpace(found) != expected {
		t.Error("Unexpected output:", found)
	}
	killer()
}
