package helpers

import (
	"os"
	"os/exec"
	"path/filepath"
	"syscall"
	"testing"
	"time"
)

// This assumes caller's CWD contains a folder named "testdata" to serve as
// the root of the www/html/public tree.

func StartServer(t *testing.T, tmpdir string) func() {
	srvLogPath := filepath.Join(tmpdir, "srv.out")
	srvLogFile, err := os.Create(srvLogPath)
	if err != nil {
		t.Fatal(err)
	}
	exe, err := exec.LookPath("go")
	if err != nil {
		t.Fatal(err)
	}
	prog := "gitlab.com/jpneverwas/urokotori/internal/soiv"
	cmd := exec.Command(
		exe, "run", prog, "-addr", "127.0.0.1:18000", "-nocrumbs", "testdata",
	)
	cmd.Stderr = srvLogFile
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	err = cmd.Start()
	if err != nil {
		t.Fatal(err)
	}
	time.Sleep(time.Second)
	return func () {
		pgid, err := syscall.Getpgid(cmd.Process.Pid)
		if err != nil {
			t.Fatal(err)
		}
		syscall.Kill(-pgid, 15)  // note the minus sign
		cmd.Wait()
	}
}
