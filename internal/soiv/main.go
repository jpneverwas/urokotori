package main

import (
	"flag"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

const listing = `
<!DOCTYPE html>
<html>
  <head>
    <title>{{.Path}}</title>
  </head>
  <body>
    <h1>
{{range encrumb .Path}}
<a href="{{ .SubPath }}">{{ .Name }}</a> /
{{ end }}
{{ lasten .Path }}
    </h1>
    <ul>
{{range .Links}}
      <li>
        <a href="{{.Href}}">{{.Text}}</a>
      </li>
{{end}}
    </ul>
  </body>
</html>
`

type Crumb struct {
	SubPath string
	Name    string
}

func makeCrumbs(path string) []Crumb {
	path = strings.TrimRight(path, "/")
	pcat := ""
	out := []Crumb{}
	if path == "" {
		return []Crumb{{"", ""}}
	}
	parts := strings.Split(path, "/")
	for _, part := range parts[:len(parts)-1] {
		if pcat != "/" {
			pcat += "/"
		}
		pcat += part
		if part == "" {
			part = "home"
		}
		out = append(out, Crumb{pcat, part})
	}
	return out
}

func makeLast(path string) string {
	parts := strings.Split(path, "/")
	return parts[len(parts)-2]
}

var tmpl *template.Template

func openSomething(path string) (*os.File, bool, error) {
	if filepath.IsAbs(path) {
		path = strings.TrimLeft(path, "/")
	}
	if path == "" {
		path = "."
	}
	f, err := os.Open(path)
	if err != nil {
		log.Println(err)
		return nil, false, err
	}
	info, err := f.Stat()
	if err != nil {
		log.Println(err)
		return nil, false, err
	}
	return f, info.IsDir(), nil
}

func handleFile(f *os.File, w http.ResponseWriter, req *http.Request) {
	written, err := io.Copy(w, f)
	if err == nil {
		log.Printf("%s -> %s (%d bytes)\n", req.Host, req.URL.Path, written)
	}
}

func handleDir(f *os.File, w http.ResponseWriter, req *http.Request) {
	if !strings.HasSuffix(req.URL.Path, "/") {
		w.Header().Add("Location", req.URL.Path+"/")
		http.Error(w, "302 Redirect", http.StatusFound)
		return
	}
	names, err := f.ReadDir(-1)
	type M struct {
		Href string
		Text string
	}
	var links []M
	for _, ent := range names {
		name := ent.Name()
		if name != "index.html" {
			if ent.IsDir() {
				name += "/"
			}
			links = append(links, M{name, name})
			continue
		}
		ff, err := os.Open(filepath.Join(f.Name(), name))
		if err != nil {
			log.Println(err)
			http.Error(w, "500 "+err.Error(), http.StatusInternalServerError)
			return
		}
		*f = *ff
		handleFile(f, w, req)
	}
	content := struct {
		Path  string
		Links []M
	}{req.URL.Path, links}
	if err = tmpl.Execute(w, content); err != nil {
		log.Println(err)
		http.Error(w, "500 "+err.Error(), http.StatusInternalServerError)
		return
	}
	log.Println(req.Host, "->", f.Name())
}

func handle(w http.ResponseWriter, req *http.Request) {
	f, isDir, err := openSomething(req.URL.Path)
	if err != nil {
		http.Error(w, "404 "+err.Error(), http.StatusNotFound)
		return
	}
	defer f.Close()
	log.Println(req.Host, "<-", req.URL.Path)
	if isDir {
		handleDir(f, w, req)
		return
	}
	handleFile(f, w, req)
}

var flagAddr = flag.String("addr", "127.0.0.1:8008", "address as host:port")

var flagNoCrumbs = flag.Bool("nocrumbs", false, "Don't print back links")

func main() {
	flag.Parse()
	origDir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	defer os.Chdir(origDir)
	path := flag.Arg(0)
	if path == "" {
		path = "."
	}
	if err = os.Chdir(path); err != nil {
		log.Fatal(err)
	}
	log.Println("Serving on", *flagAddr)

	// Setup template
	m := template.FuncMap{"encrumb": makeCrumbs, "lasten": makeLast}
	if *flagNoCrumbs {
		m["encrumb"] = func(s string) []Crumb { return nil }
	}
	tmpl = template.Must(template.New("dir").Funcs(m).Parse(listing))
	// os.Chdir(*flagAddr)
	// http.ListenAndServe(*flagAddr, http.FileServer(http.Dir(".")))
	http.ListenAndServe(*flagAddr, http.HandlerFunc(handle))
}
