package mhonarchy

import (
	"log"
	"net/http"

	"github.com/antchfx/antch"
	"github.com/antchfx/htmlquery"
)

const threadExpression = "/html/body/ul/li/ul/li/a"

// ThreadSpider is a scraper for a month's thread/date summary page
//
// The Expression slot should be something like threadExpression. To stop
// after 90 items, do "(/html/body/ul/li/ul/li)[position() < 90]" or similar.
type ThreadSpider struct {
	PostSpiderFactory func() antch.Handler
	Expression string
}

func (s *ThreadSpider) ServeSpider(c chan<- antch.Item, res *http.Response) {
	doc, err := antch.ParseHTML(res)
	if err != nil {
		panic(err)
	}
	// The tree here is confusing, leaves prone to visiting twice
	seen := make(map[string]interface{})
	for _, node := range htmlquery.Find(doc, s.Expression) {
		leaf := htmlquery.SelectAttr(node, "href")
		if _, ok := seen[leaf]; ok {
			log.Fatalf("Already seen: %s", leaf)
		}
		seen[leaf] = nil
		nextUrl, err := res.Request.URL.Parse(leaf)
		if err != nil {
			panic(err)
		}
		v := &LinkJob{
			Host: res.Request.Host,
			URL: nextUrl,
			Child: s.PostSpiderFactory(),
		}
		c <- v
	}
}

func CreateDefaultThreadSpider() antch.Handler {
	return &ThreadSpider{
		PostSpiderFactory: CreateDefaultPostSpider,
		Expression: threadExpression,
	}
}
