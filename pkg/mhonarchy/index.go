package mhonarchy

import (
	"net/http"

	"github.com/antchfx/antch"
	"github.com/antchfx/htmlquery"
)

const indexExpression = "/html/body/ul/li/a[1]"

// IndexSpider is a spider specific to Gnu archive index pages.
type IndexSpider struct {
	ThreadSpiderFactory func() antch.Handler
	Expression string
}

func (s *IndexSpider) ServeSpider(c chan<- antch.Item, res *http.Response) {
	doc, err := antch.ParseHTML(res)
	if err != nil {
		panic(err)
	}
	for _, node := range htmlquery.Find(doc, s.Expression) {
		leaf := htmlquery.SelectAttr(node, "href")
		nextUrl, err := res.Request.URL.Parse(leaf)
		if err != nil {
			panic(err)
		}
		gate := make(chan antch.Item)
		v := &LinkJob{
			Host: res.Request.Host,
			URL: nextUrl,
			Child: s.ThreadSpiderFactory(),
			Gate: gate,
		}
		c <- v
		<-gate
	}
}

func CreateDefaultIndexSpider() antch.Handler {
	return &IndexSpider{
		ThreadSpiderFactory: CreateDefaultThreadSpider,
		Expression: indexExpression,
	}
}
