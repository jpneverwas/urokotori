package mhonarchy

import (
	"encoding/csv"
	"io"

	"github.com/antchfx/antch"
)

type csvOutputPipeline struct {
	collector chan []string
	next antch.PipelineHandler
}

func CreateCSVPipeline(out io.Writer) antch.Pipeline {
	writer := csv.NewWriter(out)
	collector := make(chan []string)
	PipelineTeardownFunctions = append(PipelineTeardownFunctions, writer.Flush)
	doCollect := func() {
		for {
			writer.Write(<- collector)
			if err := writer.Error(); err != nil {
				panic(err)
			}
			writer.Flush()
		}
	}
	return func(next antch.PipelineHandler) antch.PipelineHandler {
		go doCollect()
		return &csvOutputPipeline{collector, next}
	}
}

func (p *csvOutputPipeline) ServePipeline(v antch.Item) {
	nug := v.(*Nugget)
	p.collector <- []string{nug.Link, nug.Id}
}
