package mhonarchy

import (
	"log"
	"net/http"

	"github.com/antchfx/antch"
	"github.com/antchfx/htmlquery"
)

const postExpression = "/child::comment()[starts-with(.,'X-Message-Id:')]"

// PostSpider is a leaf scraper for one article
type PostSpider struct {
	index int
	Expression string
	Attempt int
}

func (s *PostSpider) ServeSpider(c chan<- antch.Item, res *http.Response) {
	doc, err := antch.ParseHTML(res)
	if err != nil {
		panic(err)
	}
	node := htmlquery.FindOne(doc, s.Expression)
	if node == nil {
		log.Println("Missing node for:", res.Request.URL.Path)
		c <- &LinkJob{
			Host: res.Request.Host,
			URL: res.Request.URL,
			Child: &PostSpider{
				Expression: postExpression,
				Attempt: s.Attempt + 1,
			},
		}
		return
	}
	c <- &Nugget{
		Id: node.Data,
		Link: res.Request.URL.String(),
	}
}

func CreateDefaultPostSpider() antch.Handler {
	return &PostSpider{
		Expression: postExpression,
	}
}
