package mhonarchy

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"testing"

	"github.com/antchfx/antch"
)

func TestEnsureSlash(t *testing.T) {
	if EnsureSlash("http://example.com") != "http://example.com" {
		t.Error("Failed")
	}
	if EnsureSlash("http://example.com/cgi") != "http://example.com/cgi/" {
		t.Error("Failed")
	}
	if EnsureSlash("http://ok.io/index.php") != "http://ok.io/index.php" {
		t.Error("Failed")
	}
}

func TestDeburr(t *testing.T) {
	if Deburr("<foo-bar@baz>") != "foo-bar@baz" {
		t.Error("Unexpected output")
	}
	if Deburr(" foo-bar@baz ") != "foo-bar@baz" {
		t.Error("Unexpected output")
	}
	if Deburr(" <foo-bar@baz ") != "<foo-bar@baz" {
		t.Error("Unexpected output")
	}
}

const headerOk = "HTTP/1.1 200 OK\r\n" +
	"Content-Type: text/html\r\n" +
	"Content-Length: %d\r\n\r\n%s"

func TestIndexSpider(t *testing.T) {
	raw, err := ioutil.ReadFile("testdata/single/archive.html")
	if err != nil {
		t.Error(err)
	}
	u, err :=url.Parse("http://localhost/archive/")
	if err != nil {
		t.Error(err)
	}
	payload := fmt.Sprintf(headerOk, 34121, string(raw))
	res, err := http.ReadResponse(
		bufio.NewReader(strings.NewReader(payload)),
		&http.Request{Method: "GET", URL: u},
	)
	if err != nil {
		t.Error(err)
	}
	c := make(chan antch.Item)
	spider := CreateDefaultIndexSpider()
	go spider.ServeSpider(c, res)
	recvd := <- c
	result, ok := recvd.(*LinkJob)
	if ok != true {
		t.Errorf("Failed")
	}
	if result.URL.String() != "http://localhost/archive/2021-01/index.html" {
		t.Errorf("Failed %v", result)
	}
}

func TestThreadSpider(t *testing.T) {
	raw, err := ioutil.ReadFile("testdata/single/summary.html")
	if err != nil {
		t.Error(err)
	}
	u, err :=url.Parse("http://localhost/archive/2020-12/index.html")
	if err != nil {
		t.Error(err)
	}
	payload := fmt.Sprintf(headerOk, 4666, string(raw))
	res, err := http.ReadResponse(
		bufio.NewReader(strings.NewReader(payload)),
		&http.Request{Method: "GET", URL: u},
	)
	if err != nil {
		t.Error(err)
	}
	c := make(chan antch.Item)
	spider := CreateDefaultThreadSpider()
	go spider.ServeSpider(c, res)
	recvd := <- c
	result, ok := recvd.(*LinkJob)
	if ok != true {
		t.Errorf("Failed")
	}
	if result.URL.String() != "http://localhost/archive/2020-12/msg00013.html" {
		t.Errorf("Failed %v", result)
	}
}

func TestPostSpider(t *testing.T) {
	raw, err := ioutil.ReadFile("testdata/single/article.html")
	if err != nil {
		t.Error(err)
	}
	u, err :=url.Parse("http://localhost/archive/2020-12/msg00013.html")
	if err != nil {
		t.Error(err)
	}
	payload := fmt.Sprintf(headerOk, 5527, string(raw))
	res, err := http.ReadResponse(
		bufio.NewReader(strings.NewReader(payload)),
		&http.Request{Method: "GET", URL: u},
	)
	if err != nil {
		t.Error(err)
	}
	c := make(chan antch.Item)
	spider := CreateDefaultPostSpider()
	go spider.ServeSpider(c, res)
	recvd := <- c
	result, ok := recvd.(*Nugget)
	if ok != true {
		t.Errorf("Failed")
	}
	if result.Link != "http://localhost/archive/2020-12/msg00013.html" {
		t.Errorf("Failed %v", result)
	}
	if result.Id != "X-Message-Id: 87pn2q28ue.fsf@trouble.defaultvalue.org " {
		t.Errorf("Failed %v", result)
	}
}
