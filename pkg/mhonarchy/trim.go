package mhonarchy

import (
	"strings"

	"github.com/antchfx/antch"
)

// TrimOutputPipeline removes everything but the actual Message-ID.
type TrimOutputPipeline struct {
	next antch.PipelineHandler
}

func (p *TrimOutputPipeline) ServePipeline(v antch.Item) {
	nug := v.(*Nugget)
	nug.Id = strings.Replace(nug.Id, "X-Message-Id:", "", 1)
	nug.Id = strings.TrimSpace(nug.Id)
	p.next.ServePipeline(nug)
}

// CreateTrimOutputPipeline creates a new TrimOutputPipeline.
// This can be passed to antch.Crawler.UsePipeline()
func CreateTrimOutputPipeline() antch.Pipeline {
	return func (next antch.PipelineHandler) antch.PipelineHandler {
		return &TrimOutputPipeline{next: next}
	}
}
