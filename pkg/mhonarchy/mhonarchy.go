// Package mhonarchy contains helpers to extract message-ids from mailing-list
// archives created with MHonArc: https://github.com/Conservatory/MHonArc
//
// TODO unify terminology. Currently thread|page|summary and article|post
// TODO maybe add pgx or redis pipeline or proprietary cloud object storage
//
package mhonarchy

import (
	"log"
	"net/url"
	"os"
	"path"
	"regexp"
	"strings"
	"time"

	"github.com/antchfx/antch"
)

type Nugget struct {
	Id   string `json:"id"`
	Link string `json:"link"`
}

// LinkJob holds info for registering another response handler
// XXX this is wasteful because by sending the child, we're adding a handler
// instance/pattern combo for every leaf instead of leveraging the matcher
// and just having one for all x/msg vs. (x/msg1, x/msg2 ...)
type LinkJob struct {
	Host  string
	URL   *url.URL
	Child antch.Handler
	Gate chan antch.Item
}

var PipelineTeardownFunctions []func()

// RunTeardown runs all teardown handlers.
func RunTeardown() {
	for _, teardown := range PipelineTeardownFunctions {
		teardown()
	}
}

// FollowPipeline is a meta pipeline that serves as a broker for others.
type FollowPipeline struct {
	next      antch.PipelineHandler
	scheduler chan *LinkJob
	heartbeat chan int
}

// Note to self: we can always hand stuff down to other spiders when
// initializing them (providing we expand their definitions along with those
// of the data "items" we're passing around)
func (p *FollowPipeline) ServePipeline(v antch.Item) {
	switch info := v.(type) {
	case *LinkJob:
		p.scheduler <- info
	case *Nugget:
		p.heartbeat <- 1
		p.next.ServePipeline(v)
	}
}

// CreateFollowPipeline returns a pipeline for dispatching others.
func CreateFollowPipeline(
	heartbeat chan int, scheduler chan *LinkJob,
) antch.Pipeline {
	return func(next antch.PipelineHandler) antch.PipelineHandler {
		return &FollowPipeline {
			next: next, scheduler: scheduler, heartbeat: heartbeat,
		}
	}
}

// MaybeInitSocks is a helper to update crawler to connect over SOCKS.
func MaybeInitSocks(crawler *antch.Crawler) (*antch.Crawler, error) {
	socksAddr := os.Getenv("SOCKS_PROXY")
	if socksAddr == "" {
		return crawler, nil
	}
	if !strings.HasPrefix(socksAddr, "socks5") {
		socksAddr = "socks5h://" + socksAddr
	}
	proxyURL, err := url.Parse(socksAddr)
	if err != nil {
		return nil, err
	}
	return crawler.UseProxy(proxyURL), nil
}

func EnsureSlash(s string) string {
	if path.Ext(s) == "" && s[len(s)-1] != '/' {
		return s + "/"
	}
	return s
}


var msgIdPat = regexp.MustCompile("^<([^>]+)>$")

// Deburr removes angle brackets around a Message-Id
func Deburr (s string) string {
	// Remove brackets, so <my-id> -> my-id
	s = strings.TrimSpace(s)
	subs := msgIdPat.FindStringSubmatch(s)
	if len(subs) == 2 {
		return subs[1]
	}
	return s
}

func HandleOne(crawler *antch.Crawler, lj *LinkJob){
	log.Println("Requesting:", lj.URL.Path)
	crawler.Handle(lj.Host+lj.URL.Path, lj.Child)
	s := EnsureSlash(lj.URL.String())
	ct := 1
	for err := crawler.EnqueueURL(s); err != nil; err = crawler.EnqueueURL(s) {
		if !strings.Contains(err.Error(), "busy") {
			log.Fatalf("%v", err)
		}
		ct++
		time.Sleep(time.Duration(2*ct) * time.Second)
	}
}

// Throttle is a horrible workaround to limit the number of requests.
//
// XXX tl;dr I don't understand upstream (or go) enough to prevent strange
// choking issue re deadlock-like behavior when left unchecked.
func Throttle(crawler *antch.Crawler, enqueued chan *LinkJob) {
	for lj := range enqueued {
		HandleOne(crawler, lj)
	}
}
