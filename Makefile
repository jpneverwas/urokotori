
TARGET_CSV ?= /tmp/fake_csv
export INBOX ?= .inbox
export MSG_DIR ?= Maildir
export GROUP ?= dummy
export BACKEND ?= nnmaildir

TESTDATA_URL = https://gitlab.com/jpneverwas/urokotori/-/package_files/17389780/download

.PHONY: main clean kelso-test mhonarchy-test rearview-test

main: .newsrc-dribble

$(TARGET_CSV):
	test -n $(TARGET_PAGE)
	cd cmd/kelso && go run main.go -pages $(TARGET_PAGE) > $@

.newsrc-dribble mbox-dicer.elc: sanity | $(MSG_DIR) $(INBOX)
	@echo group: $(GROUP)
	@echo msgdir: $(MSG_DIR)
	@echo inbox: $(INBOX)
	@echo backend: $(BACKEND)
ifeq ($(INTERACTIVE),1)
	emacs -Q -l mbox-dicer.el \
		--eval '(setq debug-on-error t message-log-max 100000)'
else
	emacs -Q -batch -f batch-byte-compile mbox-dicer.el
	emacs -Q -batch -l mbox-dicer.elc --eval '(mbox-dicer-main)'
endif

$(INBOX): | $(MBOXES)
	@echo mboxes: $(MBOXES)
	cp -vr $(MBOXES) $(INBOX)

$(MSG_DIR):
	mkdir $@

rearview-test: | cmd/rearview/testdata
	cd cmd/rearview && go test

cmd/rearview/testdata: | extras/testdata
	cd cmd/rearview && ln -sf ../../extras/testdata .

mhonarchy-test: | pkg/mhonarchy/testdata
	cd pkg/mhonarchy && go test

pkg/mhonarchy/testdata: | extras/testdata
	cd pkg/mhonarchy && ln -sf ../../extras/testdata .

kelso-test: | cmd/kelso/testdata
	cd cmd/kelso && go test

cmd/kelso/testdata: | extras/testdata
	cd cmd/kelso && ln -sf ../../extras/testdata .

extras/testdata: extras/testdata.tar.gz
	cd extras && tar -xf testdata.tar.gz

extras/testdata.tar.gz:
	curl --silent --show-error --fail --location --output $@ $(TESTDATA_URL)

DEST = $(MSG_DIR)/$(GROUP)
ifeq ($(BACKEND),nnmaildir)
	DEST := $(DEST)/cur
endif
.nnmail-cache:
	test -d $(DEST)
	grep -ri '^message-id:' $(DEST) | awk '{printf("%s\t$(GROUP)\n", $$2)}' > $@
	chmod 600 $@

clean: sanity
	test "$$(pwd)" != "$$(echo ~)"
ifneq ($(MSG_DIR),Mail)
	rm -rvf Mail
endif
	rm -rvf News $(INBOX)
	rm -vf mbox-dicer.elc \
		.newsrc-dribble .newsrc-dribble~ '#.newsrc-dribble#' \
		.emacs-mail-crash-box .nnmail-cache \
		.newsrc.eld .newsrc.eld~ \
		mbox~ .mbox-active

clean-all: clean
	find $(MSG_DIR) -mindepth 1 -delete
	rm -f mbox

sanity:
ifeq ($(CI),true)
	@echo "Skipping sanity check"
else
	test ! -d .git
endif
