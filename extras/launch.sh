#!/bin/sh

DOCKER=$(command -v docker || command -v podman) || exit 1

test -e Maildir && set -- "$@" --volume "$(realpath Maildir):/project/Maildir:z"
test -e Mboxes && set -- "$@" --volume "$(realpath Mboxes):/project/Mboxes:z"

exec "$DOCKER" run --rm -it \
    --volume "$(realpath Makefile):/project/Makefile:z" \
    --volume "$(realpath mbox-dicer.el):/project/mbox-dicer.el:z" \
    "$@" docker.io/silex/emacs:27-ci bash -i

