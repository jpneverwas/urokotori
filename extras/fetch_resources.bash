#!/bin/bash

# These are for downloading and extracting maildirs

set -e

: "${CHECK_FS:=1}"

get_fstype() {
    df --output=fstype . | tail -n1
}

ensure_fstype() {
    (( CHECK_FS )) || return 0
    local fs
    fs=$(get_fstype)
    if [[ $fs != @(zfs|btrfs) ]]; then
        echo "File system $fs not one of btrfs, zfs"
        return 1
    fi >&2
}

download() {
    local url
    [[ ! $1 ]] && echo "Need project name (branch)" && return 1
    url=https://jpneverwas.gitlab.io/urokotori/$1/
    shift
    wget -np -H -r -D gitlab.com "$@" "$url"
    rm -rvf ./jpneverwas.gitlab.io
}

extract_links() {
    ensure_fstype
    local files
    readarray -t files < <(
        find gitlab.com -name download -o -name \*links\*release
    )
    mkdir links
    for file in "${files[@]}"; do
        [[ $file == *download && $(tar -tf "$file") != *.csv ]] && continue
        tar -xf "$file" -C links
    done
    rm -rvf ./gitlab.com
    du -hs links
}

extract_maildirs() {
    ensure_fstype
    local files
    readarray -t files < <(find gitlab.com -name download)
    for file in "${files[@]}"; do
        [[ $file == *download && $(tar -tf "$file") == *.csv ]] && continue
        mv -v "$file" ./maildir.tar.gz
        tar -xf ./maildir.tar.gz
        rm -f ./maildir.tar.gz
    done
    rm -rvf ./gitlab.com
    du -hs Maildir
}

#                                                     shellcheck disable=SC2012
renumber() {
    [[ ! $1 ]] && echo "Need project name (branch)" && return 1
    local -i total current n
    total=$(find "Maildir/$1/.nnmaildir/nov/" -mindepth 1 | wc -l)
    current=$(ls -1 "Maildir/$1/.nnmaildir/num/" | sort -n | tail -n1)
    echo "Extending $current to $total"
    while (( ++n <= total )); do
        touch "Maildir/$1/.nnmaildir/num/$n"
        (( n % 1000 )) || printf '.'
    done
    current=$(ls -1 "Maildir/$1/.nnmaildir/num/" | sort -n | tail -n1)
    (( total == current )) && echo ok
}

if (( ! $# )) || [[ $1 == @(-h|--help) ]]; then
    cat <<EOF
usage: download <branch> | extract_links | extract_maildirs | renumber <branch>
EOF
    exit 0
fi

"$@"
